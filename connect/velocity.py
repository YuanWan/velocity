import socket
import sys
import argparse
from datetime import datetime
import time
from time import gmtime, strftime

import pandas as pd
import quickfix as fix
import quickfix44 as fix44

mylist = [
    'EURUSD',
    'USDJPY',
    'GBPUSD',
    'AUDUSD',
    'NZDUSD',
    'USDCAD',
    'USDSGD',
    'EURJPY',
    'AUDJPY',
    'CADJPY',
    'EURAUD',
    'AUDCAD',
    'AUDNZD'
]


class Application(fix.Application):
    Sender_QUOTE='DYMON-S-QUOTE'
    Sender_TRADE='DYMON-S-TRADE'
    unit_amount=10000



    def gen_quote_id(self):
        return str(datetime.now())

    def gen_ord_id(self):
        return str(datetime.now())

    def onCreate(self, sessionID):
        pass
        return

    def onLogon(self, sessionID):
        print ("Successful Logon to session '%s'." % sessionID.toString())
        return

    def onLogout(self, sessionID): return


    def createDF(self):
        self.df = pd.DataFrame(index=mylist,columns=['Name', 'QuoteID','TranactTime', 'BidPx', 'OfferPx'])

    def getDF(self):
        return self.df

    def toAdmin(self, message, sessionID ):
        message.getHeader().setField(56, "CITIFX-UAT")
        message.getHeader().setField(57, "FXSpot")
        # reset seq number on login
        if message.getHeader().getField(35)=="A":
            message.setField(141, "Y")
            message.setField(554, "citifxuat")
        # print "Sent the Admin following message: %s" % message.toString()
        return

    def fromAdmin(self, message,sessionID):
        # On log in
            # 56: targetComID
        if message.getHeader().getField(35)=='A':
            if message.getHeader().getField(56)==self.Sender_QUOTE:
                self.QUOTE_Session=sessionID
            if message.getHeader().getField(56)==self.Sender_TRADE:
                self.TRADE_Session=sessionID
        if message.getHeader().getField(35)==2:
            print "Seq No. Error"
        # print "Received the Admin following message: %s" % message.toString()
        return

    def toApp(self, message,sessionID):
        print "APP Sent the following message: %s" % message.toString()
        return

    def fromApp(self, message, sessionID):
        # handle quotes
        if message.getHeader().getField(35) == 'S':
            Symbol=message.getField(55)
            QuoteID=message.getField(117)
            BidPx=message.getField(132)
            OfferPx=message.getField(133)
            TranactTime=message.getField(60)
            self.df.set_value(Symbol, 'TranactTime', TranactTime)
            self.df.set_value(Symbol, 'QuoteID', QuoteID)
            self.df.set_value(Symbol, 'BidPx', BidPx)
            self.df.set_value(Symbol, 'OfferPx', OfferPx)


        #  re-subscribe the quote if canceled by citi
        if message.getHeader().getField(35) == 'Z':
            symbol = message.getField(131)
            self.getQuote(symbol,1000)
            print "resubscribed to "+symbol
        if message.getHeader().getField(35) != 'S':
            print "APP Received the following message: %s" % message.toString()
        return


    # --------------QUOTE Session------------------

    def getQuote(self,symbol,amount):
        print "Requesting for Quote"
        quote = fix.Message()
        quote.getHeader().setField(fix.SenderCompID('DYMON-S-QUOTE'))
        quote.getHeader().setField(fix.TargetSubID('FXSpot'))
        quote.getHeader().setField(fix.MsgType('R'))
        quote.setField(fix.NoRelatedSym(1))
        quote.setField(fix.Symbol(symbol))
        quote.setField(fix.QuoteReqID(symbol))
        quote.setField(fix.QuoteRequestType(2))
        quote.setField(fix.OrderQty(amount))
        print quote.toString()
        fix.Session.sendToTarget(quote, self.QUOTE_Session)
        return

    def cancelQuote(self,symbol):
        print("Cancelling the following order: ")
        cancelQ = fix.Message()
        cancelQ.getHeader().setField(fix.SenderCompID('DYMON-S-QUOTE'))
        cancelQ.getHeader().setField(fix.TargetSubID('FXSpot'))
        cancelQ.getHeader().setField(fix.MsgType('AJ'))
        cancelQ.setField(fix.QuoteRespID("CANCEL-"+symbol))
        cancelQ.setField(fix.QuoteRespType(6))
        cancelQ.setField(fix.Symbol(symbol))
        cancelQ.setField(fix.QuoteReqID(self.QuoteReqID))
        print cancelQ.toString()
        fix.Session.sendToTarget(cancelQ, self.QUOTE_Session)




    # --------------TRADE Session------------------

# TODO: multiunit order
    def putOrder(self, side, units, symbol,price):
        #  in bip
        price_diff=5

        print("Creating the following order: ")
        trade = fix.Message()
        trade.getHeader().setField(fix.SenderCompID('DYMON-S-TRADE'))
        trade.getHeader().setField(fix.TargetSubID('FXSpot'))
        trade.getHeader().setField(fix.MsgType('D'))
        trade.setField(fix.ClOrdID(self.gen_ord_id())) #11=Unique order
        trade.setField(fix.Account('35944156'))
        trade.setField(fix.HandlInst('1'))
        trade.setField(fix.Symbol(symbol))
        if side=='B':
            trade.setField(fix.Side(fix.Side_BUY))
            offer=float(self.df.get_value(symbol, 'OfferPx'))
            if offer<=(1+price_diff/10000)*price:
                pass
            else:
                print "Quoted Offer Price too HIGH "+str(offer)
                return
        elif side == 'S':
            trade.setField(fix.Side(fix.Side_SELL))
            bid=float(self.df.get_value(symbol, 'BidPx'))
            if bid>=(1-price_diff/10000)*price:
                pass
            else:
                print "Quoted Bid Price too LOW "+str(bid)
                return
        else:
            print "invalid trading side"
            return
        trade.setField(fix.StringField(60,(datetime.utcnow().strftime ("%Y%m%d-%H:%M:%S.%f"))[:-3]))
        trade.setField(fix.OrderQty(self.unit_amount))
        trade.setField(fix.OrdType('D'))
        trade.setField(fix.TimeInForce('4'))
        trade.setField(fix.Currency('USD'))
        trade.setField(fix.QuoteID(self.df.get_value(symbol,'QuoteID')))
        print trade.toString()
        fix.Session.sendToTarget(trade, self.TRADE_Session)

#     --------Clean up----------
    def clean_up(self):
        self.QUOTE_Session=-1
        self.TRADE_Session=-1
        self.LastQuoteID=-1
        self.QuoteReqID=-1
        self.QuotePrice_BUY=-1
        self.QuotePrice_SELL = -1



class Velocity():

    def __init__(self):
        self.unit_amount = 10000
        try:
            self.settings = fix.SessionSettings("C:\Users\strategy.intern.8\PycharmProjects\Velocity\connect\settings.cfg")
            self.application = Application()
            self.application.clean_up()
            self.application.createDF()
            self.storeFactory = fix.FileStoreFactory(self.settings)
            self.logFactory = fix.FileLogFactory(self.settings)
            self.initiator = fix.SocketInitiator(self.application, self.storeFactory, self.settings, self.logFactory)
            self.initiator.start()
            # acceptor = fix.SocketAcceptor(application, storeFactory, settings, logFactory)
            # acceptor.start()
            time.sleep(5)
            for i, j in enumerate(mylist):
                self.application.getQuote(j, self.unit_amount)
            time.sleep(5)


            # while 1:
            #     input = raw_input()
            #     if input == '1':
            #         print "Quote"
            #         self.application.getQuote('EURCAD',3000)
            #     if input == '2':
            #         self.application.putOrder('EURCAD',3000,'S')
            #     if input == '3':
            #         self.application.cancelQuote('EURCAD','1123')
            #     if input == 'd':
            #         import pdb
            #         pdb.set_trace()
            #     else:
            #         continue

        except (fix.ConfigError, fix.RuntimeError), e:
            print e

    def order(self,side,unit,symbol,price):
        try:
            print "Order Generated: " +side+" "+str(unit)+" units "+symbol+" at "+str(price)
            self.application.putOrder(side, unit, symbol,price)
        except (fix.ConfigError, fix.RuntimeError), e:
            print e

    def getDF(self):
        print self.application.getDF()


